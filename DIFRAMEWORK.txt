

- dependency injection (setter alapon)
	- objektum gráf(kör nékül)
	- Setter alapon szeretnénk feloldani a függőséget
- proxies vs decorator
	- normál objektumokhoz szeretnék plusz funkciókat adni (deklaratívan)
	- Hogyan csinálnátok és miért? (proxy vs decorator vagy más mintát használnátok?)
- extendable authorization algoritms (strategy & voters)
	- DecisionManager komponens amely "votereken" keresztül dönt egy adott kérés authorizációjáról
		Több fajta stratégia támogatása
- scope handling
	- Hogyan kezeljük a szkópokat, életciklus definiálása az objektumaink számára(mikor készítsünk újat?)
- factory for object creation
	- Egységes API objektumok "elkéréséhez" a konténertől
