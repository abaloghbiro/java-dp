package hu.traning360.ds.abstractfactory;

public class FactoryProducer {

	public static AbstractFactory getFactory(String choice) {

		if (choice.equalsIgnoreCase("shape")) {
			return new ShapeFactory();
		} else if ("color".equalsIgnoreCase(choice)) {
			return new ColorFactory();
		}
		return null;
	}

	public static void main(String[] args) {

		AbstractFactory colorFactory = FactoryProducer.getFactory("color");

		Color c = colorFactory.getColor("red");

		c.fill();

		AbstractFactory shapeFactory = FactoryProducer.getFactory("shape");

		Shape s = shapeFactory.getShape("triangle");

		s.draw();

	}
}
