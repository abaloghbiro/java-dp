package hu.traning360.ds.abstractfactory;

public class Red implements Color {

	public void fill() {
		System.out.println("Color : RED");
	}

}
