package hu.traning360.ds.abstractfactory;

public interface Shape {

	void draw();
}
