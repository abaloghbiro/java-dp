package hu.traning360.ds.abstractfactory;

public interface Color {

	void fill();
}
