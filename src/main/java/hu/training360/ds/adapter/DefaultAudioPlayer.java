package hu.training360.ds.adapter;

public class DefaultAudioPlayer implements MediaPlayer {

	private MediaAdapter adapter = new MediaAdapter();
	

	public void play(String filename) {
		
		if(filename.endsWith(".wav")) {
			System.out.println("Playing audio...");
		}
		else {
			adapter.play(filename);
		}
		
	}

}
