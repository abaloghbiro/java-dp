package hu.training360.ds.adapter;

public interface MediaPlayer {

	void play(String filename);
}
