package hu.training360.ds.adapter;

public class MediaAdapter implements MediaPlayer {

	private VideoPlayer videoPlayerAdaptee;

	public MediaAdapter() {
		this.videoPlayerAdaptee = new DefaultVLCPlayer();
	}

	public void play(String filename) {

		if (filename.endsWith(".vlc")) {
			videoPlayerAdaptee.playVLC(filename);
		}
	}

}
