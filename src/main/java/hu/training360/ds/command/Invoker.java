package hu.training360.ds.command;

import java.util.ArrayList;
import java.util.List;

public class Invoker {

	private List<StockOrder> orders = new ArrayList<>();

	public void addOrder(StockOrder order) {
		this.orders.add(order);
	}

	public void executeOrders() {
		orders.forEach(o -> o.execute());
		orders.clear();
	}
}
