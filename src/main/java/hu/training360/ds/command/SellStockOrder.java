package hu.training360.ds.command;

public class SellStockOrder implements StockOrder {

	private Stock stock;

	@Override
	public void execute() {
		stock.sellStock();
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}
