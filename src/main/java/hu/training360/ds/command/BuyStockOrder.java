package hu.training360.ds.command;

public class BuyStockOrder implements StockOrder {

	private Stock stock;

	@Override
	public void execute() {
		stock.buyStock();
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

}
