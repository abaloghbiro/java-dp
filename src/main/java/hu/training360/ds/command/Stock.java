package hu.training360.ds.command;

public class Stock {

	private int stockNumber;
	private String stockCode;

	public Stock(int stockNumber, String stockCode) {
		super();
		this.stockNumber = stockNumber;
		this.stockCode = stockCode;
	}

	public int getStockNumber() {
		return stockNumber;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void sellStock() {

		if (stockNumber != 0) {
			stockNumber = stockNumber - 1;
		}

		System.out.println("Stock sold, current number: " + stockCode + " / " + stockNumber);

	}

	public void buyStock() {
		stockNumber = stockNumber + 1;
		System.out.println("Stock bought, current number: " + stockCode + " / " + stockNumber);

	}

}
