package hu.training360.ds.command;

public class Client {

	public static void main(String[] args) {

		// Receiver
		Stock stock = new Stock(100, "FB");

		// Sell command
		SellStockOrder sell = new SellStockOrder();
		sell.setStock(stock);

		// Buy command
		BuyStockOrder buy = new BuyStockOrder();
		buy.setStock(stock);

		// Invoker
		Invoker i = new Invoker();

		i.addOrder(sell);
		i.addOrder(buy);

		i.executeOrders();

	}

}
