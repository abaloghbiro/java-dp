package hu.training360.ds.prototype;

public class Rectangle implements Shape, Cloneable {

	private int width;
	private int height;

	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public void draw() {

		System.out.println("rectangle");

	}

	public Object clone() throws CloneNotSupportedException {
		return (Rectangle) super.clone();
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
