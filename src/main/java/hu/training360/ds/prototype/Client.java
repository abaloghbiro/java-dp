package hu.training360.ds.prototype;

public class Client {

	public static void main(String[] args) {

		Rectangle original = new Rectangle(10, 3);

		System.out.println("Original: " + original);
		try {
			Rectangle cloned = cloneObject(original);
			System.out.println("Cloned: " + cloned);
		} catch (CloneNotSupportedException e) {
			System.err.println(e);
		}

	}

	public static Rectangle cloneObject(Rectangle s) throws CloneNotSupportedException {

		return (Rectangle) s.clone();
	}
}
