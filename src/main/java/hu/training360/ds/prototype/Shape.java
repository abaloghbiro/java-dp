package hu.training360.ds.prototype;

public interface Shape extends Cloneable {

	void draw();
}
