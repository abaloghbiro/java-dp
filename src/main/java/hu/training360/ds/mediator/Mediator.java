package hu.training360.ds.mediator;

public interface Mediator {

	void sendMessage(String message, User sender);
}
