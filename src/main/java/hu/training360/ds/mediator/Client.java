package hu.training360.ds.mediator;

public class Client {

	public static void main(String[] args) {

		Mediator m = new DefaultMediator();
		User john = new User("John", m);
		User robert = new User("Robert", m);

		john.sendMessage();
		robert.sendMessage();
	}
}
