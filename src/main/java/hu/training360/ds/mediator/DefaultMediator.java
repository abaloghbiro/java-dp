package hu.training360.ds.mediator;

public class DefaultMediator implements Mediator {

	@Override
	public void sendMessage(String message, User sender) {

		System.out.println("Message received, sender: " + sender + " ==> " + message);

	}

}
