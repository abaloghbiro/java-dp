package hu.training360.ds.mediator;

public class User {

	private String username;

	private Mediator mediator;

	public User(String username, Mediator m) {
		this.username = username;
		this.mediator = m;
	}

	public void sendMessage() {
		mediator.sendMessage("Hello here is " + username, this);
	}

	@Override
	public String toString() {
		return "User [ "+ username + "]";
	}

}
