package hu.training360.ds.strategy;

public interface Strategy {

	int calculation(int a, int b);
}
