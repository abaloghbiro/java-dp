package hu.training360.ds.strategy;

public class StrategyContext implements Strategy {

	private Strategy strategy;

	public StrategyContext(Strategy st) {
		this.strategy = st;
	}

	@Override
	public int calculation(int a, int b) {
		return strategy.calculation(a, b);
	}

	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	@Override
	public String toString() {
		return "StrategyContext [strategy=" + strategy + "]";
	}
	
	

}
