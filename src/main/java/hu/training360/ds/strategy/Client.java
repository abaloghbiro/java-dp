package hu.training360.ds.strategy;

public class Client {

	public static void main(String[] args) {

		Strategy t1 = new AddOperationStrategy();
		Strategy t2 = new MultiplyOperationStrategy();
		Strategy t3 = new SubstractionOperationStrategy();

		StrategyContext stContext = new StrategyContext(t1);
		System.out.println(stContext.calculation(3, 2));

		stContext.setStrategy(t2);
		System.out.println(stContext.calculation(3, 2));
		stContext.setStrategy(t3);
		System.out.println(stContext.calculation(3, 2));
	}
}
