package hu.training360.ds.strategy;

public class SubstractionOperationStrategy implements Strategy {

	@Override
	public int calculation(int a, int b) {
		return a - b;
	}

	@Override
	public String toString() {
		return "SubstractionOperationStrategy []";
	}
}
