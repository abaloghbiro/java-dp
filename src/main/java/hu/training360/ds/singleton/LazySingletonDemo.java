package hu.training360.ds.singleton;

public class LazySingletonDemo {

	class Singleton {

		private Singleton instance = null;

		private Singleton() {

		}

		public synchronized Singleton getInstance() {

			if (instance == null) {
				instance = new Singleton();
			}
			return instance;
		}

	}

}
