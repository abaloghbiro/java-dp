package hu.training360.ds.singleton;

public class LazySingletonDoubleChecked {

	private static volatile LazySingletonDoubleChecked INSTANCE = null;

	private LazySingletonDoubleChecked() {

	}

	public static LazySingletonDoubleChecked getInstance() {
		if (INSTANCE == null) {
			synchronized (LazySingletonDoubleChecked.class) {
				if (INSTANCE == null) {
					INSTANCE = new LazySingletonDoubleChecked();
				}
			}
		}
		return INSTANCE;
	}

}
