package hu.training360.ds.singleton;

public class EagerSingleton {

	private static EagerSingleton INSTANCE;

	static {

		try {
			INSTANCE = new EagerSingleton();
		} catch (Exception ex) {

		}

	}

	private EagerSingleton() {

	}

	public static EagerSingleton getInstance() {
		return INSTANCE;
	}
}
