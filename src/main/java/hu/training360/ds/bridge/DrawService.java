package hu.training360.ds.bridge;

public interface DrawService {

	void draw();
}
