package hu.training360.ds.bridge;

public abstract class Shape {

	private String name;

	protected DrawService drawService;

	public Shape(DrawService drawService) {

		this.drawService = drawService;
	}

	public void draw() {
		drawService.draw();
	}

	public String getName() {
		return name;
	}

}
