package hu.training360.ds.bridge;

public class BridgeClient {

	public static void main(String[] args) {

		Circle c1 = new Circle(new GreenCircleDrawService());
		Circle c2 = new Circle(new RedCircleDrawService());

		c1.draw();
		c2.draw();
	}
}
