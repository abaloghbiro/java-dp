package hu.training360.ds.decorator;

public interface AccountService {

	boolean transfer(Double amount, String accSource, String accDest);
}
