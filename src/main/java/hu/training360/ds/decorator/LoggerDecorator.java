package hu.training360.ds.decorator;

import java.lang.reflect.Method;
import java.util.logging.Logger;

public class LoggerDecorator implements AccountService {

	private AccountService accountService;

	private Logger logger = Logger.getLogger("loggerDecorator");

	public LoggerDecorator(AccountService ser) {
		this.accountService = ser;
	}

	public boolean transfer(Double amount, String accSource, String accDest) {

		try {
			Method m = accountService.getClass().getMethod("transfer", Double.class, String.class, String.class);

			Loggable l = m.getAnnotation(Loggable.class);

			if (m.getAnnotation(Loggable.class) != null && l.isEnabled()) {

				logger.info("Amount is : " + amount);
				logger.info("AccSource: " + accSource);
				logger.info("AccDest:" + accDest);
			}
		} catch (Exception e) {
			System.err.println(e);
		}

		accountService.transfer(amount, accSource, accDest);

		return true;

	}

}
