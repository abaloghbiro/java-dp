package hu.training360.ds.decorator;

public interface MailService {

	void sendEmail(String emailContent);
}
