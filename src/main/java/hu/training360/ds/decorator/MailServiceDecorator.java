package hu.training360.ds.decorator;

import java.lang.reflect.Method;
import java.util.logging.Logger;

public class MailServiceDecorator implements AccountService, MailService {

	private AccountService accountService;

	private Logger logger = Logger.getLogger("loggerDecorator");

	public MailServiceDecorator(AccountService ser) {
		this.accountService = ser;
	}

	public boolean transfer(Double amount, String accSource, String accDest) {

		try {
			Method m = accountService.getClass().getMethod("transfer", Double.class, String.class, String.class);

			Loggable l = m.getAnnotation(Loggable.class);

			if (m.getAnnotation(Loggable.class) != null && l.isEnabled()) {

				logger.info("Amount is : " + amount);
				logger.info("AccSource: " + accSource);
				logger.info("AccDest:" + accDest);
			}
		} catch (Exception e) {
			System.err.println(e);
		}

		accountService.transfer(amount, accSource, accDest);

		sendEmail("Transfer was ok!");

		return true;

	}

	@Override
	public void sendEmail(String emailContent) {
		System.out.println("Email sent: " + emailContent);
	}

}
