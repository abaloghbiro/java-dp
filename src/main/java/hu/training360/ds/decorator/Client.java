package hu.training360.ds.decorator;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		AccountService s1 = new DefaultAccountService();
		
		s1.transfer(2000d, "11111", "333333");
		
		
		AccountService s2 = new LoggerDecorator(s1);
		
		s2.transfer(2001d, "11112", "333333");
	}

}
