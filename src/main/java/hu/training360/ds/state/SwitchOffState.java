package hu.training360.ds.state;

public class SwitchOffState implements TVState {

	@Override
	public void doAction() {
		System.out.println("TV switch off!");
	}

}
