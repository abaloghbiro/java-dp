package hu.training360.ds.state;

public class SwitchOnState implements TVState {

	@Override
	public void doAction() {
		System.out.println("TV switch on!");
	}

}
