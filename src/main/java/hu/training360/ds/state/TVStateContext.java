package hu.training360.ds.state;

public class TVStateContext implements TVState {

	private TVState state;

	public TVStateContext(TVState state) {
		this.state = state;
	}

	@Override
	public void doAction() {
		this.state.doAction();
	}

	public TVState getState() {
		return state;
	}

	public void setState(TVState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "TVStateContext [state=" + state + "]";
	}

}
