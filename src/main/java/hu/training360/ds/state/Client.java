package hu.training360.ds.state;

public class Client {

	public static void main(String[] args) {

		TVState switchOn = new SwitchOnState();

		TVStateContext controller = new TVStateContext(switchOn);

		controller.doAction();

		TVState switchOff = new SwitchOffState();

		controller.setState(switchOff);

		controller.doAction();

	}

}
