package hu.training360.ds.observer;

public abstract class Observer {

	protected Subject subject;

	public abstract void update();
}
