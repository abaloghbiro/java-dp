package hu.training360.ds.composite;

import java.util.ArrayList;
import java.util.List;

public class Employee {

	private String name;
	private Integer age;
	private Double salary;
	private List<Employee> employees = new ArrayList<Employee>();

	public Employee(String name, Integer age, Double salary) {
		super();
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void addEmployee(Employee e) {
		employees.add(e);
	}

	public void removeEmployee(Employee e) {
		employees.remove(e);
	}

	public String getName() {
		return name;
	}

	public Integer getAge() {
		return age;
	}

	public Double getSalary() {
		return salary;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + ", salary=" + salary + ", employees=" + employees + "]";
	}
	
	

}
