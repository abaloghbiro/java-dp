package hu.training360.ds.composite;

public class App {

	public static void main(String[] args) {

		Employee e1 = new Employee("ceo", 30, 999999.0);

		Employee e2 = new Employee("headOfMarketing", 30, 800000.0);
		Employee e3 = new Employee("headOfDev", 30, 800000.0);

		e1.addEmployee(e2);
		e1.addEmployee(e3);

		Employee e4 = new Employee("marketingManager", 30, 700000.0);
		Employee e5 = new Employee("softwareDeveloper", 30, 700000.0);

		e2.addEmployee(e4);
		e3.addEmployee(e5);

		System.out.println(e1);
		for (Employee e : e1.getEmployees()) {
			System.out.println(e);
			for (Employee ee : e.getEmployees()) {
				System.out.println(ee);
			}
		}

	}

}
