package hu.training360.ds.factory;

public class AbstractProduct {

	protected Integer code;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
