package hu.training360.ds.factory;

public class ProductFactory {

	public static AbstractProduct createProductByCode(Integer code) {

		AbstractProduct product = null;
		if (1 == code) {
			product = new ProductOne();
		} else if (2 == code) {
			product = new ProductTwoo();
		}
		if (product != null) {
			product.setCode(code);
			return product;
		}
		throw new IllegalArgumentException("Unknown code!");
	}

	public static void main(String[] args) {

		AbstractProduct productOne = ProductFactory.createProductByCode(1);

		System.out.println(productOne.getCode());

		AbstractProduct productTwoo = ProductFactory.createProductByCode(2);

		System.out.println(productTwoo.getCode());
	}
}
