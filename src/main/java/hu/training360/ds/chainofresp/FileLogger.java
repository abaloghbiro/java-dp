package hu.training360.ds.chainofresp;

public class FileLogger extends AbstractLogger {

	public FileLogger(AbstractLogger nextLogger) {
		super(AbstractLogger.DEBUG, nextLogger);
	}

	@Override
	protected void write(String message) {
		System.out.println("File logger: " + message);
	}

}
