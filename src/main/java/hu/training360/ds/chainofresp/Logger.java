package hu.training360.ds.chainofresp;

public interface Logger {

	void logMessage(String message, int level);
}
