package hu.training360.ds.chainofresp;

public abstract class AbstractLogger implements Logger {

	public static int INFO = 1;
	public static int DEBUG = 2;
	public static int ERROR = 3;

	protected int loggerLevel;
	protected AbstractLogger nextLogger;

	public AbstractLogger(int loggerLevel, AbstractLogger nextLogger) {
		this.loggerLevel = loggerLevel;
		this.nextLogger = nextLogger;
	}

	public void logMessage(String message, int level) {

		if (loggerLevel >= level) {
			write(message);
		} else {

			if (nextLogger != null) {
				nextLogger.logMessage(message, level);
			} else {
				System.out.println("No handler in the chain");
			}

		}
	}

	protected abstract void write(String message);
}
