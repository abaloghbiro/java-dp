package hu.training360.ds.chainofresp;

public class ErrorLogger extends AbstractLogger {

	public ErrorLogger(AbstractLogger nextLogger) {
		super(AbstractLogger.ERROR, nextLogger);
	}

	@Override
	protected void write(String message) {
		System.out.println("Error logger: " + message);
	}

}
