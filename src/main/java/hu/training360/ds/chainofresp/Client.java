package hu.training360.ds.chainofresp;

public class Client {

	public static void main(String[] args) {

		Logger logger = getLoggerChain();
		logger.logMessage("Hello", AbstractLogger.ERROR);
	}

	public static Logger getLoggerChain() {
		return new ConsoleLogger(new FileLogger(new ErrorLogger(null)));
	}
}
