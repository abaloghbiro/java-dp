package hu.training360.ds.chainofresp;

public class ConsoleLogger extends AbstractLogger {

	public ConsoleLogger(AbstractLogger nextLogger) {
		super(AbstractLogger.INFO, nextLogger);
	}

	@Override
	protected void write(String message) {

		System.out.println("Console logger: " + message);

	}

}
