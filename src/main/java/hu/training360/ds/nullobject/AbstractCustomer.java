package hu.training360.ds.nullobject;

public abstract class AbstractCustomer {

	protected String name;

	public abstract boolean isPresent();

	public abstract String getName();
}
