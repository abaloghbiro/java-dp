package hu.training360.ds.nullobject;

public class NullCustomer extends AbstractCustomer {

	@Override
	public String getName() {
		return "Not Available in Customer Database";
	}

	@Override
	public boolean isPresent() {
		return false;
	}

}