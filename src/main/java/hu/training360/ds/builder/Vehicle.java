package hu.training360.ds.builder;

public class Vehicle {

	private String type;
	private String color;
	private Integer numberOfDoors;
	private Double engineSize;
	private String chasseyType;
	private Integer numberOfSeats;
	private String brand;

	private Vehicle(Vehicle.VehicleBuilder builder) {

		this.type = builder.type;
		this.color = builder.color;
		this.numberOfDoors = builder.numberOfDoors;
		this.engineSize = builder.engineSize;
		this.chasseyType = builder.chasseyType;
		this.numberOfSeats = builder.numberOfSeats;
		this.brand = builder.brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getNumberOfDoors() {
		return numberOfDoors;
	}

	public void setNumberOfDoors(Integer numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}

	public Double getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(Double engineSize) {
		this.engineSize = engineSize;
	}

	public String getChasseyType() {
		return chasseyType;
	}

	public void setChasseyType(String chasseyType) {
		this.chasseyType = chasseyType;
	}

	public Integer getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(Integer numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public static class VehicleBuilder {

		private String type;
		private String color;
		private Integer numberOfDoors;
		private Double engineSize;
		private String chasseyType;
		private Integer numberOfSeats;
		private String brand;

		public VehicleBuilder(String brand, String type) {
			this.brand = brand;
			this.type = type;
		}

		public VehicleBuilder setColor(String arg) {
			this.color = arg;
			return this;
		}

		public VehicleBuilder setNumOfDoors(String arg) {
			this.color = arg;
			return this;
		}

		public VehicleBuilder setEngineSize(String arg) {
			this.color = arg;
			return this;
		}

		public VehicleBuilder setChasseyType(String arg) {
			this.color = arg;
			return this;
		}

		public VehicleBuilder setNumOfSeats(String arg) {
			this.color = arg;
			return this;
		}

		public Vehicle build() {
			return new Vehicle(this);
		}
	}

	public static void main(String[] argd) {

		VehicleBuilder builder = new VehicleBuilder("opel", "vectra");

		Vehicle v = builder.setNumOfSeats("4").setChasseyType("hatchback").setColor("black").setEngineSize("1.5")
				.setNumOfDoors("5").build();

		System.out.println(v.toString());

	}
}
