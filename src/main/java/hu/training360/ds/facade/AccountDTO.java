package hu.training360.ds.facade;

public class AccountDTO {

	private String accountId;
	private String bankAccountNumber;
	private Double initialBalance;
	private String userId;
	private Double limit;

	public AccountDTO(String bankAccountNumber, Double initialBalance, String userId) {
		super();
		this.bankAccountNumber = bankAccountNumber;
		this.initialBalance = initialBalance;
		this.userId = userId;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public Double getInitialBalance() {
		return initialBalance;
	}

	public String getUserId() {
		return userId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Double getLimit() {
		return limit;
	}

	public void setLimit(Double limit) {
		this.limit = limit;
	}

}
