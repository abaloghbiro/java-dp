package hu.training360.ds.facade;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AccountManager {

	private static volatile AccountManager INSTANCE = null;

	private Map<String, AccountDTO> accounts = new HashMap<>();

	private AccountManager() {

	}

	public static AccountManager getInstance() {
		if (INSTANCE == null) {
			synchronized (AccountManager.class) {
				if (INSTANCE == null) {
					INSTANCE = new AccountManager();
				}
			}
		}
		return INSTANCE;
	}

	public void createBankAccount(AccountDTO dto) {

		dto.setAccountId(UUID.randomUUID().toString());
		accounts.put(dto.getAccountId(), dto);
	}

	public AccountDTO getBankAccount(String accountId) {
		AccountDTO acc = accounts.get(accountId);

		if (acc == null) {
			throw new IllegalArgumentException("Account not found!");
		}

		return acc;
	}
}
