package hu.training360.ds.facade;

public class DefaultBankAccountFacade implements BankAccountFacade {

	@Override
	public void createBankAccount(AccountDTO data) {

		AccountPermission pm = SecurityManager.getInstance().getAccPermission();

		if (pm.hasPermission(data.getUserId())) {
			AccountManager.getInstance().createBankAccount(data);
		} else {
			throw new SecurityException("You are not authorized to create account!");
		}

	}

	@Override
	public void setLimit(String bankAccountId, Double limit) {

		AccountDTO dto = AccountManager.getInstance().getBankAccount(bankAccountId);

		if (dto == null) {
			throw new IllegalArgumentException("Account not found");
		}

		dto.setLimit(limit);

	}

}
