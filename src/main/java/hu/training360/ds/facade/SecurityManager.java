package hu.training360.ds.facade;

public class SecurityManager {

	private static volatile SecurityManager INSTANCE = null;

	private static final AccountPermission pm = new AccountPermission();

	private SecurityManager() {

	}

	public static SecurityManager getInstance() {
		if (INSTANCE == null) {
			synchronized (SecurityManager.class) {
				if (INSTANCE == null) {
					INSTANCE = new SecurityManager();
				}
			}
		}
		return INSTANCE;
	}

	public AccountPermission getAccPermission() {
		AccountPermission permission = null;
		try {
			permission = (AccountPermission) pm.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}

		return permission;
	}
}
