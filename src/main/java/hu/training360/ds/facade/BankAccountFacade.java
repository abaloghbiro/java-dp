package hu.training360.ds.facade;

public interface BankAccountFacade {

	void createBankAccount(AccountDTO data);

	void setLimit(String bankAccountId, Double limit);
}
