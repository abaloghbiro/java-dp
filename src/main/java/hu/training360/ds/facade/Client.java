package hu.training360.ds.facade;

public class Client {

	
	public static void main(String[]args) {
		
		
		BankAccountFacade f = new DefaultBankAccountFacade();
		
		AccountDTO dto = new AccountDTO("111111", 1000d, "user1");
		
		f.createBankAccount(dto);
		
		f.setLimit(dto.getAccountId(), 2000d);
		
		System.out.println(f);
	}
}
