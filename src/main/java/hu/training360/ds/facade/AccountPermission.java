package hu.training360.ds.facade;

import java.util.ArrayList;
import java.util.List;

public class AccountPermission implements Cloneable {

	private List<String> userIds = new ArrayList<>();

	public AccountPermission() {
		userIds.add("user1");
		userIds.add("user2");
	}

	public boolean hasPermission(String userId) {
		return this.userIds.contains(userId);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
