package hu.training360.ds.proxy;

public interface LoggerService {

	void logRequest(String message);
}
