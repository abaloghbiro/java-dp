package hu.training360.ds.proxy;

public class Client {

	
	public static void main(String[]args) {
		
		
		LoggerService service = new LoggerCacheProxy(new ConsoleLoggerService());
		
		service.logRequest("1");
		service.logRequest("2");
		service.logRequest("3");
		service.logRequest("4");
		service.logRequest("5");
		service.logRequest("6");
		service.logRequest("7");
		service.logRequest("8");
		service.logRequest("9");
		service.logRequest("10");

		
	}
}
