package hu.training360.ds.proxy;

import java.util.logging.Logger;

public class ConsoleLoggerService implements LoggerService {

	private static final Logger LOGGER = Logger.getLogger("consoleLogger");

	public void logRequest(String message) {
		LOGGER.info("Message received: " + message);
	}

}
