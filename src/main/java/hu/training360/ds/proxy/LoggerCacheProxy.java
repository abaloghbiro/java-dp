package hu.training360.ds.proxy;

import java.util.ArrayList;
import java.util.List;

public class LoggerCacheProxy implements LoggerService {

	private LoggerService target;
	private List<String> messages = new ArrayList<String>();

	public LoggerCacheProxy(LoggerService service) {
		this.target = service;
	}

	public void logRequest(String message) {

		if (messages.size() > 9) {
			messages.add(message);
		} else {
			messages.forEach(m -> target.logRequest(m));
			messages.clear();
		}

	}

}
