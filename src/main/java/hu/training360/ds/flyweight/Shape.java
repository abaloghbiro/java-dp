package hu.training360.ds.flyweight;

public interface Shape {
	void draw();
}
